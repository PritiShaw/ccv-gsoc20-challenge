from flask import Flask, request
from flask_marshmallow import Marshmallow
import os
import json
from sqlalchemy import or_
from db import db, app, Posts
import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from flask_graphql import GraphQLView


ma = Marshmallow(app)

# Post Schema


class PostSchema(ma.Schema):
    class Meta:
        fields = ('id', 'post_type_id', 'creation_date', 'score', 'body', 'owner_user_id', 'last_activity_date', 'comment_count', 'parent_id', 'view_count',
                  'title', 'tags', 'answer_count', 'last_editor_user_id', 'last_edit_date', 'accepted_answer_id', 'favorite_count', 'owner_display_name', 'closed_date')


class CustomIDNode(graphene.relay.Node):
    class Meta:
        name = 'IDNode'

    @staticmethod
    def to_global_id(type, id):
        # returns a non-encoded ID
        return id

    @staticmethod
    def get_node_from_global_id(info, global_id, only_type=None):
        model = getattr(graphene.Query, info.field_name).field_type._meta.model
        return model.objects.get(id=global_id)


class PostObject(SQLAlchemyObjectType):
    class Meta:
        model = Posts
        interfaces = (CustomIDNode, )


class GraphQLquery(graphene.ObjectType):
    """To see all the posts, example query 
        ```
        {
            allPosts {
                edges {
                node {
                    id
                    body
                    title
                    creationDate
                }
                }
            }
        }
        ```
    """
    all_posts = SQLAlchemyConnectionField(PostObject)


graphql_schema = graphene.Schema(query=GraphQLquery)

# Init schema
Post_schema = PostSchema()
Posts_schema = PostSchema(many=True)


@app.route('/post/<id>', methods=['GET'])
def get_Post(id):
    Post = Posts.query.get(id)
    return Post_schema.jsonify(Post)


@app.route('/posts', methods=['GET'])
def get_Posts():
    sort_by = request.args.get('orderBy')
    sort_by_array = []
    if sort_by is None or len(sort_by) < 3:
        sort_by_array.append(Posts.creation_date.desc())
    else:
        for parameter in sort_by.split(','):
            if len(parameter) < 3:
                continue
            parameter_array = parameter.strip().lower().split(":")
            order_param = parameter_array[0]
            order_dir = -1 if (len(parameter_array) == 1
                               or parameter_array[1] == "desc") else 1
            if order_param == "views":
                sort_by_array.append(Posts.view_count.asc(
                ) if order_dir == 1 else Posts.view_count.desc())

            elif order_param == "score":
                sort_by_array.append(Posts.score.asc(
                ) if order_dir == 1 else Posts.view_count.desc())

            elif order_param == "date":
                sort_by_array.append(Posts.creation_date.asc(
                ) if order_dir == 1 else Posts.view_count.desc())

    search_term = request.args.get('search')
    if search_term is None:
        search_term = ""
    search = "%{}%".format(search_term)
    posts = Posts.query.filter(or_(Posts.title.like(
        search), Posts.body.like(search))).order_by(*sort_by_array).all()

    return Posts_schema.jsonify(posts)


app.add_url_rule(
    '/graphql',
    view_func=GraphQLView.as_view(
        'graphql',
        schema=graphql_schema,
        graphiql=True  # for having the GraphiQL interface
    )
)

if __name__ == '__main__':
    app.run(debug=True)
