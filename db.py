from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# Init app
app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
# Database
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
    os.path.join(basedir, "ingest", 'db.sqlite')
# Init db
db = SQLAlchemy(app)


class Posts(db.Model):

    # Required Fields
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(20000), nullable=False)
    comment_count = db.Column(db.Integer, nullable=False)
    creation_date = db.Column(db.DateTime, nullable=False)
    last_activity_date = db.Column(db.DateTime, nullable=False)
    owner_user_id = db.Column(db.Integer, nullable=False)
    post_type_id = db.Column(db.Integer, nullable=False)
    score = db.Column(db.Integer, nullable=False)
    
    # Optional Fields
    accepted_answer_id = db.Column(db.Integer)
    answer_count = db.Column(db.Integer)
    closed_date = db.Column(db.DateTime)
    favorite_count = db.Column(db.Integer)
    last_edit_date = db.Column(db.DateTime)
    last_editor_user_id = db.Column(db.Integer)
    owner_display_name = db.Column(db.String(15))
    parent_id = db.Column(db.Integer)
    tags = db.Column(db.String(100))
    title = db.Column(db.String(200))
    view_count = db.Column(db.Integer)

    def __init__(self, post):  
        # Required Fields      
        self.id = post["id"]
        self.body = post["body"]
        self.comment_count = post["comment_count"]
        self.creation_date = post["creation_date"]
        self.last_activity_date = post["last_activity_date"]
        self.owner_user_id = post["owner_user_id"]
        self.post_type_id = post["post_type_id"]
        self.score = post["score"]

        # Optional Fields
        self.accepted_answer_id = post.get("accepted_answer_id")
        self.answer_count = post.get("answer_count")
        self.closed_date = post.get("closed_date")
        self.favorite_count = post.get("favorite_count")
        self.last_edit_date = post.get("last_edit_date")
        self.last_editor_user_id = post.get("last_editor_user_id")
        self.owner_display_name = post.get("owner_display_name")
        self.parent_id = post.get("parent_id")
        self.tags = post.get("tags")
        self.title = post.get("title")
        self.view_count = post.get("view_count")

# create database
db.create_all()