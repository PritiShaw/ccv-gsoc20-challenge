import os
import sys
import re
from datetime import datetime
import xml.etree.ElementTree as ET
from flask_sqlalchemy import SQLAlchemy
sys.path.append('../')
from db import db, Posts


if len(sys.argv) != 2:
    print("Please mention the Path to XML")
    quit()
xml_path = sys.argv[1]

try:
    # Ingesting
    tree = ET.parse(xml_path)
    root = tree.getroot()
    datetime_format = "%Y-%m-%dT%H:%M:%S" # millisecond inconsistency in raw XML
    datetime_attribs = ["creation_date",
                        "last_activity_date", "closed_date", "last_edit_date"]
    for row in root:
        attributes = {}
        for attrib in row.attrib:
            formatted_attrib = re.sub(r'(?<!^)(?=[A-Z])', '_', attrib).lower()
            value = row.get(attrib)
            if formatted_attrib in datetime_attribs:
                # Handled millisecond inconsistency by trimming it
                value = datetime.strptime(value[:-4], datetime_format)   
            attributes[formatted_attrib] = value
        new_Post = Posts(attributes)
        db.session.add(new_Post)
    db.session.commit()
    print("Successfully ingested the XML file")
    
except Exception as e:
    print("Error:", e, file=sys.stderr)
