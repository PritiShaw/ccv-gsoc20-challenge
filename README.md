Canadian Common CV Selection test
---
Deployed at https://ccv-task.herokuapp.com/

[Pipenv](https://pypi.org/project/pipenv/) has been used for creating virtual environment.
## Ingest XML file

The raw XML file ( example in `ingest/bioinformatics_posts_se.xml`)  can be ingested to form the required SQLite database required for functioning of the web application. Following steps are to be followed for ingesting:

``` bash
# Activate venv
pipenv shell

# Install dependencies
pipenv install

cd ingest 

# ingesting the file
python ingest.py <PATH to XML File>
```

## Installation Process

``` bash
# Activate venv
pipenv shell

# Install dependencies
# Skip if already installed for ingesting XML file
pipenv install

# Run REST API Server (http://localhost:5000)
python app.py
```

## API Endpoint

* GET     `/posts`
Parameters to be sent as URL query string
**Parameters**

	* *search*
		It searches the provided string (case insensitive) in the `body` or `title` attribute of the `Posts`. 
		For example `/posts?search=merging`
		
	* *orderBy*
		Values needs to be in format `<attribute>:<order>`
		**attribute** can be *score, views, date*, if not provided *date* is considered
		**order** can be *asc* for ascending or *desc* for descending, if not provided *desc* is considered
		Attributes can be clubbed by using `,` in between them, for example:
		`posts?orderBy=score:asc,views:desc` will give ascending score and then descending views.

##  GraphQL API

GraphiQL is visible in browser when a **GET** request is sent to endpoint `/graphql`  
Sending **POST** request to this endpoint will give response in JSON
[GraphQL](https://graphql.org/) lets you to interact with the database, it gives clients the power to ask for exactly what they need and nothing else.  
Example GraphQL query
```
{
    allPosts {
        edges {
        node {
            id
            body
            title
            creationDate
        }
        }
    }
}
```
